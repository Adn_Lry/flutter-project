import 'dart:convert';
import 'dart:async';
import 'dart:developer';
import 'package:flutter/material.dart' as prefix0;
import 'package:flutter_app/selectedItem.dart';

import 'request.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();

}

class _MyAppState extends prefix0.State<MyApp> {
  //controleur pour récupérer le texte des textview
  final locationController = TextEditingController();
  final queryController = TextEditingController();

  //contient le résultat de la requete
  Future<Result> res;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Foursquare',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Foursquare'),
        ),
        body: prefix0.ListView(children: <Widget>[
            builResearchForm(),
            getResults(),
          ],
          shrinkWrap: true,
          )
        )
    );
  }

  /**
   * Contruit le formulaire de recherche
   */
  Widget builResearchForm(){
    return Column(
      children: <Widget>[
        Text("Location :"),
        TextField(controller: locationController,
        decoration: InputDecoration(
            hintText: 'Entrer une location',
        )),
        Text("Type de lieu (ex : bar, restaurant) : "),
        TextField(controller: queryController,
          decoration: InputDecoration(
          hintText: 'Entrer un type'
        )),
        RaisedButton(
          onPressed: () async{
          setState(() {
            res = fetchPost();
          });
          },
          child: Text("Rechercher"),
        ),
      ],
    );
  }

  /**
   * récupère le résultat de la requete et construit une listview
   */
  FutureBuilder<Result> getResults(){
    return FutureBuilder<Result>(
          future: res,
          builder: (context, snapshot) {
    if (snapshot.hasData) {
      List<Venue> venuesList = snapshot.data.response.venues.toList();
      return _buildSuggestions(venuesList);
    }else if(snapshot.hasError) {
      return Text("${snapshot.error}");
    }
    // By default, show a loading spinner.
    return CircularProgressIndicator();
    });
  }

  /**
   * Construit la listview a partir de la liste des resultats
   */
  Widget _buildSuggestions(List<Venue> venuesList) {
    return ListView.builder(
        padding: const EdgeInsets.all(16),
        itemCount: venuesList.length,
        itemBuilder: (BuildContext _context, int i) {
          if (i.isOdd) {
            return Divider();
          }
          return _buildRow(venuesList[i]);
        },
      shrinkWrap: true,
    );
  }

  /**
   * COnstruit un élément de la listview
   */
  Widget _buildRow(Venue venue) {
    return ListTile(
      title: Text(
        venue.name,
      ),
      onTap: () {
        Navigator.push(context, prefix0.MaterialPageRoute(
          builder : (context) => SelectedItem(),
        ));
      },
    );
  }

  /**
   * Lance la requete selon les valeurs du formulaire de recherche
   */
  Future<Result> fetchPost() async {
    log("Date du jour : "+TimeOfDay.now().toString());

    if(queryController.text.toString().length == 0 || locationController.text.toString().length == 0){
      return null;
    }

    final link = 'https://api.foursquare.com/v2/venues/search?'
        'client_id=2HVQKZY22QAM03KHJGWJXS5E213NQTMUNEGDSXZKOIZ5KF4T'
        '&client_secret=H4V3DT5YQRNCP3GZX2NPMVR113V3LAY1NPC5U0CKI5I0332V'
        '&query='+queryController.text.toString()+
        '&distance=5'
        '&near='+locationController.text.toString()+
        '&v=20191104';
    final response = await http.get(link);

    if (response.statusCode == 200) {
      var parsedResponse = json.decode(response.body);
      print(parsedResponse);
      Result res = new Result.fromJson(parsedResponse);
      return res;
    } else {
      throw Exception('Failed to load post');
    }
  }
}
