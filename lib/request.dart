// To parse this JSON data, do
//
//     final result = resultFromJson(jsonString);

import 'dart:convert';

Result resultFromJson(String str) => Result.fromJson(json.decode(str));

String resultToJson(Result data) => json.encode(data.toJson());

class Result {
  Meta meta;
  Response response;

  Result({
    this.meta,
    this.response,
  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    meta: Meta.fromJson(json["meta"]),
    response: Response.fromJson(json["response"]),
  );

  Map<String, dynamic> toJson() => {
    "meta": meta.toJson(),
    "response": response.toJson(),
  };
}

class Meta {
  int code;
  String requestId;

  Meta({
    this.code,
    this.requestId,
  });

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
    code: json["code"],
    requestId: json["requestId"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "requestId": requestId,
  };
}

class Response {
  List<Venue> venues;
  Geocode geocode;

  Response({
    this.venues,
    this.geocode,
  });

  factory Response.fromJson(Map<String, dynamic> json) => Response(
    venues: List<Venue>.from(json["venues"].map((x) => Venue.fromJson(x))),
    geocode: Geocode.fromJson(json["geocode"]),
  );

  Map<String, dynamic> toJson() => {
    "venues": List<dynamic>.from(venues.map((x) => x.toJson())),
    "geocode": geocode.toJson(),
  };
}

class Geocode {
  String what;
  String where;
  Feature feature;
  List<dynamic> parents;

  Geocode({
    this.what,
    this.where,
    this.feature,
    this.parents,
  });

  factory Geocode.fromJson(Map<String, dynamic> json) => Geocode(
    what: json["what"],
    where: json["where"],
    feature: Feature.fromJson(json["feature"]),
    parents: List<dynamic>.from(json["parents"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "what": what,
    "where": where,
    "feature": feature.toJson(),
    "parents": List<dynamic>.from(parents.map((x) => x)),
  };
}

class Feature {
  Cc cc;
  CityEnum name;
  String displayName;
  String matchedName;
  String highlightedName;
  int woeType;
  String slug;
  String id;
  String longId;
  Geometry geometry;

  Feature({
    this.cc,
    this.name,
    this.displayName,
    this.matchedName,
    this.highlightedName,
    this.woeType,
    this.slug,
    this.id,
    this.longId,
    this.geometry,
  });

  factory Feature.fromJson(Map<String, dynamic> json) => Feature(
    cc: ccValues.map[json["cc"]],
    name: cityEnumValues.map[json["name"]],
    displayName: json["displayName"],
    matchedName: json["matchedName"],
    highlightedName: json["highlightedName"],
    woeType: json["woeType"],
    slug: json["slug"],
    id: json["id"],
    longId: json["longId"],
    geometry: Geometry.fromJson(json["geometry"]),
  );

  Map<String, dynamic> toJson() => {
    "cc": ccValues.reverse[cc],
    "name": cityEnumValues.reverse[name],
    "displayName": displayName,
    "matchedName": matchedName,
    "highlightedName": highlightedName,
    "woeType": woeType,
    "slug": slug,
    "id": id,
    "longId": longId,
    "geometry": geometry.toJson(),
  };
}

enum Cc { FR }

final ccValues = EnumValues({
  "FR": Cc.FR
});

class Geometry {
  Centre center;
  Bounds bounds;

  Geometry({
    this.center,
    this.bounds,
  });

  factory Geometry.fromJson(Map<String, dynamic> json) => Geometry(
    center: Centre.fromJson(json["center"]),
    bounds: Bounds.fromJson(json["bounds"]),
  );

  Map<String, dynamic> toJson() => {
    "center": center.toJson(),
    "bounds": bounds.toJson(),
  };
}

class Bounds {
  Centre ne;
  Centre sw;

  Bounds({
    this.ne,
    this.sw,
  });

  factory Bounds.fromJson(Map<String, dynamic> json) => Bounds(
    ne: Centre.fromJson(json["ne"]),
    sw: Centre.fromJson(json["sw"]),
  );

  Map<String, dynamic> toJson() => {
    "ne": ne.toJson(),
    "sw": sw.toJson(),
  };
}

class Centre {
  double lat;
  double lng;

  Centre({
    this.lat,
    this.lng,
  });

  factory Centre.fromJson(Map<String, dynamic> json) => Centre(
    lat: json["lat"].toDouble(),
    lng: json["lng"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "lat": lat,
    "lng": lng,
  };
}

enum CityEnum { NANTES, SAINT_HERBLAIN }

final cityEnumValues = EnumValues({
  "Nantes": CityEnum.NANTES,
  "Saint-Herblain": CityEnum.SAINT_HERBLAIN
});

class Venue {
  String id;
  String name;
  Location location;
  List<Category> categories;
  ReferralId referralId;
  bool hasPerk;
  VenuePage venuePage;

  Venue({
    this.id,
    this.name,
    this.location,
    this.categories,
    this.referralId,
    this.hasPerk,
    this.venuePage,
  });

  factory Venue.fromJson(Map<String, dynamic> json) => Venue(
    id: json["id"],
    name: json["name"],
    location: Location.fromJson(json["location"]),
    categories: List<Category>.from(json["categories"].map((x) => Category.fromJson(x))),
    referralId: referralIdValues.map[json["referralId"]],
    hasPerk: json["hasPerk"],
    venuePage: json["venuePage"] == null ? null : VenuePage.fromJson(json["venuePage"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "location": location.toJson(),
    "categories": List<dynamic>.from(categories.map((x) => x.toJson())),
    "referralId": referralIdValues.reverse[referralId],
    "hasPerk": hasPerk,
    "venuePage": venuePage == null ? null : venuePage.toJson(),
  };
}

class Category {
  Id id;
  CategoryName name;
  PluralName pluralName;
  ShortName shortName;
  Icon icon;
  bool primary;

  Category({
    this.id,
    this.name,
    this.pluralName,
    this.shortName,
    this.icon,
    this.primary,
  });

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: idValues.map[json["id"]],
    name: categoryNameValues.map[json["name"]],
    pluralName: pluralNameValues.map[json["pluralName"]],
    shortName: shortNameValues.map[json["shortName"]],
    icon: Icon.fromJson(json["icon"]),
    primary: json["primary"],
  );

  Map<String, dynamic> toJson() => {
    "id": idValues.reverse[id],
    "name": categoryNameValues.reverse[name],
    "pluralName": pluralNameValues.reverse[pluralName],
    "shortName": shortNameValues.reverse[shortName],
    "icon": icon.toJson(),
    "primary": primary,
  };
}

class Icon {
  String prefix;
  Suffix suffix;

  Icon({
    this.prefix,
    this.suffix,
  });

  factory Icon.fromJson(Map<String, dynamic> json) => Icon(
    prefix: json["prefix"],
    suffix: suffixValues.map[json["suffix"]],
  );

  Map<String, dynamic> toJson() => {
    "prefix": prefix,
    "suffix": suffixValues.reverse[suffix],
  };
}

enum Suffix { PNG }

final suffixValues = EnumValues({
  ".png": Suffix.PNG
});

enum Id { THE_4_BF58_DD8_D48988_D1_D2941735, THE_4_BF58_DD8_D48988_D111941735 }

final idValues = EnumValues({
  "4bf58dd8d48988d111941735": Id.THE_4_BF58_DD8_D48988_D111941735,
  "4bf58dd8d48988d1d2941735": Id.THE_4_BF58_DD8_D48988_D1_D2941735
});

enum CategoryName { BAR_SUSHIS, RESTAURANT_JAPONAIS }

final categoryNameValues = EnumValues({
  "Bar à sushis": CategoryName.BAR_SUSHIS,
  "Restaurant japonais": CategoryName.RESTAURANT_JAPONAIS
});

enum PluralName { BARS_SUSHIS, RESTAURANTS_JAPONAIS }

final pluralNameValues = EnumValues({
  "Bars à sushis": PluralName.BARS_SUSHIS,
  "Restaurants japonais": PluralName.RESTAURANTS_JAPONAIS
});

enum ShortName { SUSHI, JAPONAISE }

final shortNameValues = EnumValues({
  "Japonaise": ShortName.JAPONAISE,
  "Sushi": ShortName.SUSHI
});

class Location {
  String address;
  double lat;
  double lng;
  List<LabeledLatLng> labeledLatLngs;
  String postalCode;
  Cc cc;
  CityEnum city;
  State state;
  Country country;
  List<String> formattedAddress;

  Location({
    this.address,
    this.lat,
    this.lng,
    this.labeledLatLngs,
    this.postalCode,
    this.cc,
    this.city,
    this.state,
    this.country,
    this.formattedAddress,
  });

  factory Location.fromJson(Map<String, dynamic> json) => Location(
    address: json["address"] == null ? null : json["address"],
    lat: json["lat"].toDouble(),
    lng: json["lng"].toDouble(),
    labeledLatLngs: List<LabeledLatLng>.from(json["labeledLatLngs"].map((x) => LabeledLatLng.fromJson(x))),
    postalCode: json["postalCode"] == null ? null : json["postalCode"],
    cc: ccValues.map[json["cc"]],
    city: json["city"] == null ? null : cityEnumValues.map[json["city"]],
    state: json["state"] == null ? null : stateValues.map[json["state"]],
    country: countryValues.map[json["country"]],
    formattedAddress: List<String>.from(json["formattedAddress"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "address": address == null ? null : address,
    "lat": lat,
    "lng": lng,
    "labeledLatLngs": List<dynamic>.from(labeledLatLngs.map((x) => x.toJson())),
    "postalCode": postalCode == null ? null : postalCode,
    "cc": ccValues.reverse[cc],
    "city": city == null ? null : cityEnumValues.reverse[city],
    "state": state == null ? null : stateValues.reverse[state],
    "country": countryValues.reverse[country],
    "formattedAddress": List<dynamic>.from(formattedAddress.map((x) => x)),
  };
}

enum Country { FRANCE }

final countryValues = EnumValues({
  "France": Country.FRANCE
});

class LabeledLatLng {
  Label label;
  double lat;
  double lng;

  LabeledLatLng({
    this.label,
    this.lat,
    this.lng,
  });

  factory LabeledLatLng.fromJson(Map<String, dynamic> json) => LabeledLatLng(
    label: labelValues.map[json["label"]],
    lat: json["lat"].toDouble(),
    lng: json["lng"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "label": labelValues.reverse[label],
    "lat": lat,
    "lng": lng,
  };
}

enum Label { DISPLAY }

final labelValues = EnumValues({
  "display": Label.DISPLAY
});

enum State { PAYS_DE_LA_LOIRE }

final stateValues = EnumValues({
  "Pays de la Loire": State.PAYS_DE_LA_LOIRE
});

enum ReferralId { V_1570633609 }

final referralIdValues = EnumValues({
  "v-1570633609": ReferralId.V_1570633609
});

class VenuePage {
  String id;

  VenuePage({
    this.id,
  });

  factory VenuePage.fromJson(Map<String, dynamic> json) => VenuePage(
    id: json["id"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
  };
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
